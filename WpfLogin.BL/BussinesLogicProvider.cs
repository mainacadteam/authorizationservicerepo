﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfLogin.BL.Helpers;
using WpfLogin.Entities.Models;

namespace WpfLogin.BL
{
    public static class BussinesLogicProvider
    {
        public static bool RegisterUser(User user)
        {
            List<User> users;
            try
            {
                users = SerializationHelper.Deserialize();
            }
            catch (Exception ex)
            {
                users = new List<User>();
            }
            foreach (User u in users)
            {
                if (u.Login == user.Login)
                {
                    return false;
                }
            }
            users.Add(user);
            SerializationHelper.Serialize(users);
            return true;
        }
    }
}
