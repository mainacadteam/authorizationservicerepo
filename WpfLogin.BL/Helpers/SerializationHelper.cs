﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WpfLogin.Entities.Models;

namespace WpfLogin.BL.Helpers
{
    public static class SerializationHelper
    {
        private static XmlSerializer formatter = new XmlSerializer(typeof(List<User>));

        static public void Serialize(List<User> users)
        {
            using (FileStream fs = new FileStream("users.xml", FileMode.Create))
            {
                try
                {
                    formatter.Serialize(fs, users);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        static public List<User> Deserialize()
        {
            using (FileStream fs = new FileStream("users.xml", FileMode.OpenOrCreate))
            {
                try
                {
                    return (List<User>)formatter.Deserialize(fs);
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
    }
}
